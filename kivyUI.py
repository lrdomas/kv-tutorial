from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.boxlayout import BoxLayout
from kivy.core.window import Window
import kivy
from kivy.config import Config


class LoginUI(BoxLayout):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def connectBtn(self):
        username = self.ids.usernameInput.text
        password = self.ids.passwordInput.text
        print(f"{username}" +
              "," + f"{password}")

        self.createPopUp("Received the credentials: ", "Username: " +
                         username + "\n" + "Password: " + password)

    def createPopUp(self, title, msg):
        box = BoxLayout(orientation='vertical', padding=(10))
        box.add_widget(Label(text=msg))
        btn1 = Button(text="Ok")

        box.add_widget(btn1)

        popup = Popup(title=title, title_size=(30), title_align='center', content=box, size_hint=(
            None, None), size=(430, 200), auto_dismiss=True)

        btn1.bind(on_press=popup.dismiss)
        popup.open()


class Login(App):
    def build(self):
        kivy.config.Config.set('graphics', 'resizable', 0)
        Config.write()
        Window.size = (600, 400)
        self.title = 'KV-Example'
        return LoginUI()


if __name__ == "__main__":
    Login().run()

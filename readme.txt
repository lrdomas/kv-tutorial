https://kivy.org/doc/stable/installation/installation-windows.html (windows)
https://kivy.org/doc/stable/installation/installation-osx.html (macOS)
https://kivy.org/doc/stable/installation/installation-linux.html (linux)


Follow the directions under: Installing the kivy stable release (These are all windows commands)

************install python 3.7.7 (PYTHON VERSION 3.8 WILL NOT WORK WITH KIVY)*************

py -m pip install --upgrade pip wheel setuptools

py -m pip install kivy==1.11.1

(Optional examples)
py -m pip install kivy_examples==1.11.1

if you are receiving errors install these dependencies: 
python -m pip install docutils pygments pypiwin32 kivy.deps.sdl2 kivy.deps.glew